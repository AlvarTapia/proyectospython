import StateChartBuilder as SCB
import SVGlib

estado1 = SCB.State("hola")

estadoDentro = SCB.State("dentro")
estadoTambien = SCB.State("tambien")
estadoMasDentro = SCB.State("mas dentro")

estado1.addState(estadoDentro)
estado1.addState(estadoTambien)

estadoDentro.addState(estadoMasDentro)

aut = SCB.Automata("holaaaaa")
aut.statesInside.append(estado1)

estado2 = SCB.State("hey")
aut.statesInside.append(estado2)

aut.newTransition(estado1, estado2)
aut.newTransition(estado2, estadoDentro)
aut.newTransition(estadoTambien, estadoMasDentro)

aut.exportStateChart("auxeState")

"""
canvas = SVGlib.Svg( (0,0), 500, 500)
canvas2 = SVGlib.Svg( (1,1), 500, 500)

#canvas.addObject(canvas2)

with open("prueba.html", "w") as f:
	f.write(str(canvas))
	f.write(str(canvas2))
"""