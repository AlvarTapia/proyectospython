"""
	Alvar Tapia Ruiz
	Lenguajes Formales
	2017
"""

"""
Para que compartieran esfuerzos, te propongo que hagas una aplicacion
para dibujar state charts utilizando el lenguaje SVG:
"""

"TODO homogeneizar variables/vocabulario"
class Svg:
	"""Clase dedicada a la creacion y manipulacion de 'capas' en las que se puede dibujar objetos SVG"""
	canvasCount = 0
	@staticmethod
	def setCanvasID():
		"""Asigna a cada canvas un identificador unico para poder reconocerlo"""
		Svg.canvasCount += 1
		return (Svg.canvasCount-1)

	def __init__(self, initialPoint, height, width,  isCanvas=False):
		"""Inicializa el canvas"""
		self.canvasID = "Layer_"+str(Svg.setCanvasID())
		self.initialPoint = initialPoint
		self.height = height
		self.width = width
		self.objectsList = []
		self.isCanvas = isCanvas

	@staticmethod
	def arrow():
		"""Establece el grafico punta de arrow para que pueda ser utilizado"""
		string = "<defs>\n"
		string += '<marker id="arrowPoint" markerWidth="13" markerHeight="13" refX="2" refY="6"orient="auto">\n<path d="M2,2 L2,11 L10,6 L2,2" fill="black" />\n</marker>\n'
		string += "</defs>\n"
		return string

	def initSVG(self):
		"""Crea la etiqueta del canvas del dibujo"""
		string = "<svg "
		string += "id"+igualComillas(self.canvasID)+" "
		string += "x"+igualComillas(self.initialPoint[0])+" "
		string += "y"+igualComillas(self.initialPoint[1])+" "
		string += "width"+igualComillas(self.width)+" "
		string += "height"+igualComillas(self.height)+" "
		string += ">\n"
		#if(self.isCanvas):
		string += Svg.arrow()
		return string

#	def __cmp__(self, other):
#		"""Hace que todos los objetos con sorting_number<100 esten bajo este plano"""
#		if hasattr(other, "getKey"):
#			return self.getKey().__cmp__(other.getKey())
#
#	def getKey(self):
#		"""Continuacion de cmp"""
#		return 100
	def addObject(self, object):
		"""Anade un nuevo object al canvas"""
		self.objectsList.append(object)

	@staticmethod
	def finishSVG():
		"""Termina la etiqueta SVG"""
		return "</svg>\n"

	def __str__(self):
		"""Imprime todo su contenido en formato SVG"""
		text = self.initSVG()
		for o in self.objectsList:
			text += str(o)
		text += self.finishSVG()
		return text


class FrameHTML:
	"""Clase dedicada a la exportacion de un dibujo SVG en HTML (facil checkeo de la imagen)"""
	def __init__(self, newCanvas):
		"""Inicializo el HTML con el canvas con el que quiere formar la imagen a exportar"""
		self.canvas = newCanvas

	@staticmethod
	def initHTML():
		"""Pone la cabecera HTML del documento"""
		return "<!DOCTYPE html>\n<html>\n<body>\n"

	def canvas(newCanvas):
		"""Establece el canvas con el que se quiere formar la imagen a exportar"""
		self.canvas = newCanvas

	@staticmethod
	def finishHTML():
		"""Pone el final de un documento HTML"""
		return "</body>\n</html>\n"

	def __str__(self):
		"""Retorna todo su contenido para que se pueda exportar en un documento HTML"""
		text = self.initHTML()
		text += str(self.canvas)
		text += self.finishHTML()
		return text


def igualComillas(value):
	"""Metodo auxiliar que encierra el valor entre comillas con un = a la izda"""
	return '="'+str(value)+'"'


class Circle:
	"""Crea un object circulo dentro del canvas"""

	def __init__(self, initialPoint, radius, stroke="black", stroke_width="5", color="yellow"):
		"""Inicializa el circulo"""
		#self.id = id
		self.r = radius
		self.centerX = initialPoint[0]
		self.centerY = initialPoint[1]
		self.stroke = stroke
		self.stroke_width = stroke_width
		self.fill = color

	def changeCenter(self, cx, cy):
		"""Cambia la ubicacion del centro en el canvas"""
		self.centerX = cx
		self.centerY = cy

	def changeRadius(self, radius):
		"""Cambia el radius del circulo"""
		self.r = radius

#	def __cmp__(self, other):
#		"""Hace que todos los objects con sorting_number<0 esten bajo este object"""
#		if hasattr(other, "getKey"):
#			return self.getKey().__cmp__(other.getKey())
#
#	def getKey(self):
#		"""Continuacion de cmp"""
#		return 0

	def __str__(self):
		"""Imprime todo su contenido en formato SVG"""
		string = "<circle "
		#string += "id"+igualComillas(self.id)+" "
		string += "cx"+igualComillas(self.centerX)+" "
		string += "cy"+igualComillas(self.centerY)+" "
		string += "r"+igualComillas(self.r)+" "
		string += "stroke"+igualComillas(self.stroke)+" "
		string += "stroke_width"+igualComillas(self.stroke_width)+" "
		string += "fill"+igualComillas(self.fill)+" "
		string += "/>\n"
		return string

class Rectangle:
	def __init__(self, initialPoint, height, width, circlePerCent=20, stroke="black", stroke_width="1", color="yellow"):
		self.initialPoint = initialPoint
		self.circlePerCent = circlePerCent
		self.width = width
		self.height = height
		self.stroke = stroke
		self.stroke_width = stroke_width
		self.color = color
	
	def __str__(self):
		string = "<rect "
		string += "x"+igualComillas(self.initialPoint[0])+" "
		string += "y"+igualComillas(self.initialPoint[1])+" "
		string += "rx"+igualComillas(self.circlePerCent)+" "
		string += "ry"+igualComillas(self.circlePerCent)+" "
		string += "width"+igualComillas(self.width)+" "
		string += "height"+igualComillas(self.height)+" "
		string += "stroke"+igualComillas(self.stroke)+" "
		string += "stroke_width"+igualComillas(self.stroke_width)+" "
		string += "fill"+igualComillas(self.color)+" "
		string += "/>\n"
		return string
		
		
class Text:
	"""Crea un texto dentro del canvas"""
	def __init__(self, initialPoint, text, color="black"):
		"""Inicializa el texto"""
		self.initialPoint = initialPoint
		self.text = text
		self.color = color

#	def __cmp__(self, other):
#		"""Hace que todos los objects con sorting_number<0 esten bajo este object"""
#		if hasattr(other, "getKey"):
#			return self.getKey().__cmp__(other.getKey())

#	def getKey(self):
#		"""Continuacion de cmp"""
#		return 0

	def __str__(self):
		"""Retorna todo su contenido en formato SVG"""
		string = "<text "
		string += "x"+igualComillas(self.initialPoint[0])+" "
		string += "y"+igualComillas(self.initialPoint[1])+" "
		string += "fill"+igualComillas(self.color)+" "
		string += ">"+str(self.text)
		string += "</text>\n"
		return string

class Path:
	"""Crea una linea (tanto recta como curva) dentro del canvas
	Que sea curva dependera del valor que se le ha dado al 'anchor'"""
	def __init__(self, initialPoint, finalPoint, anchor=(), stroke="black", stroke_width="7"):
		self.initialPoint = initialPoint
		self.finalPoint = finalPoint
		self.anchor = anchor
		self.stroke = stroke
		self.stroke_width = stroke_width

#	def __cmp__(self, other):
#		"""Hace que todos los objects con sorting_number<-100 esten bajo este object"""
#		if hasattr(other, "getKey"):
#			return self.getKey().__cmp__(other.getKey())

#	def getKey(self):
#		"""Continuacion de cmp"""
#		return -100

	def __str__(self):
		string = "<path "
		string += 'd="'
		string += 'M '+str(self.initialPoint[0])+","+str(self.initialPoint[1])+" "
		if self.anchor:
			string += 'Q '+str(self.anchor[0])+","+str(self.anchor[1])+" "
		string += str(self.finalPoint[0])+","+str(self.finalPoint[1])
		string += '" '
		string += "stroke"+igualComillas(self.stroke)+" "
		string += "stroke_width"+igualComillas(self.stroke_width)+" "
		string += 'fill="none" '
		string += 'marker-end="url(#arrowPoint)"'
		string += "/>\n"
		return string

class BrokenLine:
	"""Crea una linea recta discontinua"""
	def __init__(self, initialPoint, finalPoint, stroke="black", stroke_width="7"):
		self.initialPoint = initialPoint
		self.finalPoint = finalPoint
		self.stroke = stroke
		self.stroke_width = stroke_width
		self.stroke_dasharray = '5,5'

	def __str__(self):
		string = "<line "
		string += "x1"+igualComillas(self.initialPoint[0])+" "
		string += "y1"+igualComillas(self.initialPoint[1])+" "
		string += "x2"+igualComillas(self.finalPoint[0])+" "
		string += "y2"+igualComillas(self.finalPoint[1])+" "
		string += "stroke-dasharray"+igualComillas(self.stroke_dasharray)+" "
		string += "stroke"+igualComillas(self.stroke)+" "
		string += "stroke_width"+igualComillas(self.stroke_width)+" "
		string += "/>\n"
		return string
		
