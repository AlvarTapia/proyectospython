import SVGlib
import copy
import math

class State:
	PROPORTION = 15
	DOUBLEPROPORTION = 2*PROPORTION
	WORDPROPORTION = 8
	def __init__(self, name):
		self.name = name
		self.height = State.DOUBLEPROPORTION
		self.width = len(self.name)*State.WORDPROPORTION
		
		self.canvas = SVGlib.Svg((0,0), self.height, self.width)
		
		self.figure = SVGlib.Rectangle( (0,0), self.height, self.width ) 
		self.text = SVGlib.Text( (State.PROPORTION,State.PROPORTION), self.name )
		
		self.statesInside = []
		
		self.contactPointsList = []
		self.contactPoints()
	
	def isStateInMe(self, stateToSearch):
		isInYou = False
		listOfStates = [self]
		if stateToSearch == self:
			isInYou = True
		else: 
			for s in self.statesInside:
				isInYou, returnList = s.isStateInMe(stateToSearch)
				if isInYou:
					for i in returnList:
						listOfStates.append(i)
					break
					
		return isInYou, listOfStates
	
	def isSuperstate(self):
		"""DEPRECATED
		Comprueba si el estado es un superestado (si tiene estados contenidos en el)"""
		if self.statesInside:
			return True
		else:
			return False
	
	def __str__(self):
		"""Imprime el estado"""
		self.update()
		self.canvas.addObject(self.figure)
		self.canvas.addObject(self.text)
		for o in self.statesInside:
			self.canvas.addObject(o)
		return str(self.canvas)
	
	def contactPoints(self):
		
		"""Recalcula los 'puntos de contacto' del estado"""
		self.contactPointsList = []
		
		self.contactPointsList.append( (self.width/2, 0) )
		self.contactPointsList.append( (self.width/2, self.height) )
		self.contactPointsList.append( (0, self.height/2) )
		self.contactPointsList.append( (self.width, self.height/2) )
		
		return self.contactPointsList
	
	def addState(self, state):
		"""Anhade un estado al estado actual"""
		if isinstance(state, State):
			self.statesInside.append(state)
	
	def update(self):
		"""Redimensiona el estado para que todos los subestados puedan caber"""
		#Calcula la nueva altura y anchura
		computeHeight = State.DOUBLEPROPORTION
		computeWidth = len(self.name)*State.WORDPROPORTION
		
		#self.statesInside = copy.deepcopy(self.statesInside)
		for o in self.statesInside:
			o.update()
			computeHeight += State.PROPORTION + o.height #Guarda la suma de las alturas + cte
			
			if o.width > computeWidth: #Si existe alguna anchura mayor a la actual
				computeWidth = o.width #Se guarda
		
		if computeHeight > self.height: #Si se computa que la altura ha aumentado
			self.height = computeHeight #Se guarda la nueva altura
		
		self.width = computeWidth+State.DOUBLEPROPORTION #Se guarda la nueva anchura
		
		#Redimensiona todos los elementos del estado actual
		self.resize()
		
		#Redistribuye los estados para que no se pisen entre ellos
		self.rearrange()
	
	def resize(self):
		"""Cambia el tamanho del canvas y del rectangulo que establecen el estado"""
		self.canvas.height = self.height
		self.canvas.width = self.width
		
		self.figure.height = self.height
		self.figure.width = self.width
		
	def rearrange(self):
		"""Recoloca todos los estados para que no se pisen entre ellos"""
		nextHeight = State.DOUBLEPROPORTION
		
		for o in self.statesInside:
			o.canvas.initialPoint = (State.PROPORTION, nextHeight)
			nextHeight += o.height + State.PROPORTION

		
class Automata:
	PROPORTION = 20
	DOUBLEPROPORTION = 2*PROPORTION
	WORDPROPORTION = 10
	def __init__(self, name):
		self.name = name
		self.height = Automata.DOUBLEPROPORTION
		self.width = len(name)*Automata.WORDPROPORTION
		
		self.canvas = SVGlib.Svg((0,0), self.height, self.width)
		self.text = SVGlib.Text( (Automata.PROPORTION,Automata.PROPORTION), self.name )
		#self.whiteboard = SVGlib.Svg((1,1), self.height, self.width)
		
		self.statesInside = []
		self.stateTransitions = []
		
	def update(self):
		"""Redimensiona el estado para que todos los subestados puedan caber"""
		#Calcula la nueva altura y anchura
		computeHeight = Automata.DOUBLEPROPORTION
		computeWidth = len(self.name)*Automata.WORDPROPORTION
		
		#self.statesInside = copy.deepcopy(self.statesInside)
		for o in self.statesInside:
			o.update()
			
			if o.height > computeHeight: #Si existe alguna anchura mayor a la actual
				computeHeight = o.height #Se guarda
			
			computeWidth += Automata.DOUBLEPROPORTION + o.width #Guarda la suma de las alturas + cte
			
			
		self.height = computeWidth+Automata.DOUBLEPROPORTION #Se guarda la nueva anchura
		#TODO bien puesta la documentacion
		if computeWidth > self.width: #Si se computa que la altura ha aumentado
			self.width = computeWidth #Se guarda la nueva altura
		
		#Redimensiona todos los elementos del estado actual
		self.resize()
		
		#Redistribuye los estados para que no se pisen entre ellos
		self.rearrange()
	
	def __str__(self):
		"""Imprime el automata"""
		string = ""
		self.update()
		
		self.canvas.addObject(self.text)
		
		for o in self.statesInside:
			self.canvas.addObject(o)
		
		for o in self.stateTransitions:
			#self.whiteboard.addObject(o)
			self.canvas.addObject(self.establishTransition( o[0], o[1] ))
		#self.canvas.addObject(self.whiteboard)
		
		string += str(self.canvas)
		return string
		
	def resize(self):
		self.canvas.height = self.height
		#self.whiteboard.height = self.height
		
		self.canvas.width = self.width
		#self.whiteboard.width = self.width
		
	def rearrange(self):
		"""Recoloca todos los estados para que no se pisen entre ellos"""
		nextWidth = Automata.PROPORTION
		
		for o in self.statesInside:
			o.canvas.initialPoint = (nextWidth, Automata.DOUBLEPROPORTION)
			nextWidth += o.width + Automata.DOUBLEPROPORTION
	
	def exportStateChart(self, fileName):
		with open( str(fileName)+".html" , "w") as f:
			f.write(str(self))
	
	def newTransition(self, initialState, targetState):
		if isinstance(initialState, State) and isinstance(targetState, State):
			self.stateTransitions.append( (initialState, targetState) )
	
	def establishTransition(self, initialState, targetState):
		#Miro a ver si los estados estan dentro del automata
		initialIsInAutomata, targetIsInAutomata = False, False
		for s in self.statesInside:
			initialIsInAutomata, initialWhere = s.isStateInMe(initialState)
			if initialIsInAutomata:
				break
		
		for s in self.statesInside:
			targetIsInAutomata, targetWhere = s.isStateInMe(targetState)
			if targetIsInAutomata:
				break
		
		#Si no estan lo notifico y no hago nada
		if not initialIsInAutomata:
			print "Initial state not found in the automata, no transition"
			return
		if not targetIsInAutomata:
			print "Target state not found in the automata, no transition"
			return
		
		#Si estan, debo calcular el 'equipaje' que llevan en las coordenadas para hacer bien la transicion
		initialXBaggage = 0
		initialYBaggage = 0
		for i in initialWhere:
			initialXBaggage += i.canvas.initialPoint[0]
			initialYBaggage += i.canvas.initialPoint[1]
			
		#print "Baggage de inicial", initialXBaggage, initialYBaggage
		
		targetXBaggage = 0
		targetYBaggage = 0
		for t in targetWhere:
			targetXBaggage += t.canvas.initialPoint[0]
			targetYBaggage += t.canvas.initialPoint[1]
			
		#print "Baggage de target", targetXBaggage, targetYBaggage
			
		#print "Ready to read first state"
		initialStateContact = initialState.contactPoints()
		#print "Ready to read second state"
		targetStateContact = targetState.contactPoints()
		
		min = float("inf")
		dupleTuple = ((),())
		for a in initialStateContact:
			for b in targetStateContact:
				initialPoint = (a[0] + initialXBaggage, a[1] + initialYBaggage)
				
				targetPoint = (b[0] + targetXBaggage, b[1] + targetYBaggage)
				
				squaredX = (initialPoint[0]-targetPoint[0])**2
				squaredY = (initialPoint[1]-targetPoint[1])**2
				
				sum = squaredX + squaredY
				
				squareRoot = math.sqrt(sum)
				
				if min > squareRoot:
					min = squareRoot
					dupleTuple =  ( initialPoint, targetPoint)
					#print "Esta es la suma "+str(sum)+", y esta la dupla"+str(dupleTuple)
		
		return SVGlib.Path( dupleTuple[0], dupleTuple[1], )